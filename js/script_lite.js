
function fmin(list){
  var min=list[0];
  for(var i = 0+1;i < list.length;i++){
    if(list[i]<min )min=list[i];
  }
  return min;
}
function fmax(list){
  var max=list[0];
  for(var i = 0+1;i < list.length;i++){
    if(list[i]>max )max=list[i];
  }
  return max;
}

function adjust_dispsize(center, adjust){
  var i = adjust > 3 ? 0 : 1; //adjust：4桁期待なら0　3桁期待なら1を
  var str='';
  if (String(center).length == 6-i) {
    str = '' + String(center);
  } else if (String(center).length == 5-i) {
    str = '_' + String(center);
  } else if (String(center).length == 4-i) {
    str = '__' + String(center);
  } else if (String(center).length == 3-i) {
    str = '___' + String(center);
  } else {
    str = '___0.0';
  }
  return str;
}

// 数値判定
function is_num(str) {
  const pattern = /^[+,-]?([1-9]\d*|0)(\.\d+)?$/
  return pattern.test(str);
}

//センサー値取得
function get_sensordata(values, sensorNum, tmp, offset) {
  for (var i = 0; i < sensorNum; i++) {

    if ((i + offset) < tmp.length && is_num(tmp[i + offset])) {
      values[i] = parseInt(tmp[i + offset]);
    }
    // return values;
  }
}

/**電源温度関連**/
const CBatteryTempMaxNum = 4;
const CthBatTmpSteady = 20; //■常時フライト前
const CthBatTmpUsual = 30; //平常ホバリング
const CthBatTmpCaution = 40; //注意
const CthBatTmpWorning = 60; //警告
const CthBatTmpDenger = 80; //危険

/**プロペラ回転数関連**/
const CPropellerNum = 8;
const CthRpmSteady = 1000; //■常時フライト前
const CthRpmUsual = 2000; //平常ホバリング
const CthRpmCaution = 2500; //注意
const CthRpmWorning = 3000; //警告
const CthRpmDenger = 3500; //危険

/** 高度計 **/
const CAltDispTypeMaxNum = 1;//表示種別数
const CAltDispMinValue = 0;
const CAltDispMaxValue = 1;

/** 電源 **/
const CBatteryMaxNum = 8; //バッテリー最大数

/** 電流値 **/
const CDispCurMaxValue = 220; //表示用最大値
const CDispCurMinValue = 0;   //表示用最小値
const CthCurSteady = 20;  //■常時フライト前
const CthCurUsual = 90;  //平常ホバリング
const CthCurCaution = 120; //注意
const CthCurWorning = 150; //警告
const CthCurDenger = 200;  //危険

/**電圧モニター関連**/
const CDispVMaxValue = 105; //表示用最大値
const CDispVMinValue = 95;  //表示用最小値
const CthVoltSteady = 103; //■常時フライト前
const CthVoltUsual = 98;  //平常ホバリング
const CthVoltCaution = 95;  //注意
const CthVoltWorning = 93;  //警告
const CthVoltDenger = 90;  //危険

const CAtlMaxNUM = 1;


/**************************/
//  ●●●●各種センサーの通信と
/**************************/
//列番号(0始まり：カラム数+1)
const ofst_Vol_Temp = 3;
const ofst_Rpm = 7;
const ofst_Vol = 15;
const ofst_Cur = 23;
const ofst_Alt = 36;
//ofst_Alt(高度値の位置)31:センサー1, 34:後方高度, 35:前方高度, 36:中心高度, 37:role角, 38:pitch角

var VatTemps = [];
var Currents = [];
var Voltages = [];
var Rpms = [];
var Altitudes = [];
var snsrTypes = {
  Temp: 0,
  rpm: 1,
  volt: 2,
  curr: 3,
  Alti: 4,
  typeNum: 5
};
var eSensorNum = [CBatteryTempMaxNum, CPropellerNum, CBatteryMaxNum, CBatteryMaxNum, CAltDispTypeMaxNum];
var eOffset = [ofst_Vol_Temp, ofst_Rpm, ofst_Vol, ofst_Cur, ofst_Alt];

//■要メンテンンス
var req = new XMLHttpRequest();
req.onreadystatechange = function () {
  if (req.readyState == 4) { // 通信の完了時
    if (req.status <= 200) { // 通信の成功時
      let val = req.responseText;
      if (!val || val.length <= 0) {
        return
      }
      const startTime = Date.now(); // 開始時間かを表示する

      let tmp = val.split(',');
      //let offset;
      // console.log(tmp);

      get_sensordata(VatTemps, eSensorNum[snsrTypes.Temp], tmp, eOffset[snsrTypes.Temp]);
      get_sensordata(Rpms, eSensorNum[snsrTypes.rpm], tmp, eOffset[snsrTypes.rpm]);
      get_sensordata(Voltages, eSensorNum[snsrTypes.volt], tmp, eOffset[snsrTypes.volt]);
      get_sensordata(Currents, eSensorNum[snsrTypes.curr], tmp, eOffset[snsrTypes.curr]);
      get_sensordata(Altitudes, eSensorNum[snsrTypes.Alti], tmp, eOffset[snsrTypes.Alti]);

      if (0) {
        for (var i = 0; i < eSensorNum[snsrTypes.Temp]; i++)console.log(VatTemps[i]);
        for (var i = 0; i < eSensorNum[snsrTypes.rpm]; i++)console.log(Rpms[i]);
        for (var i = 0; i < eSensorNum[snsrTypes.volt]; i++)console.log(Voltages[i]);
        for (var i = 0; i < eSensorNum[snsrTypes.curr]; i++)console.log(Currents[i]);
        for (var i = 0; i < eSensorNum[snsrTypes.Alti]; i++)console.log(Altitudes[i]);
      }

      //温度
      // document.getElementById("tempnvalue").innerText = Math.min(Currents);
      // document.getElementById("tempxvalue").innerText = Math.max(Currents);

      //電流
      document.getElementById("curnvalue").innerText = adjust_dispsize(fmin(Currents).toFixed(1),3);
      document.getElementById("curxvalue").innerText = adjust_dispsize(fmax(Currents).toFixed(1),3);

      //電圧
      document.getElementById("volnvalue").innerText = adjust_dispsize(fmin(Voltages).toFixed(1),3);
      document.getElementById("volxvalue").innerText = adjust_dispsize(fmax(Voltages).toFixed(1),3);

      //プロペラ回転速度
      document.getElementById("rpmnvalue").innerText = fmin(Rpms).toFixed(1);
      document.getElementById("rpmxvalue").innerText = fmax(Rpms).toFixed(1);

      //高度計：値表示
      var center = Altitudes[0].toFixed(1);
      //document.getElementById("altvalue").innerText = alt_output(center, 4);
      
      if (String(center).length == 6) {
        document.getElementById("altvalue").innerText = '' + String(center);
      } else if (String(center).length == 5) {
        document.getElementById("altvalue").innerText = '_' + String(center);
      } else if (String(center).length == 4) {
        document.getElementById("altvalue").innerText = '__' + String(center);
      } else if (String(center).length == 3) {
        document.getElementById("altvalue").innerText = '___' + String(center);
      } else {
        document.getElementById("altvalue").innerText = '___0.0';
      }
      
      const endTime = Date.now(); // 終了時間
      console.log(endTime - startTime); // 何ミリ秒かかった
    }
  }
}
// data.csvに書き込んだデータをDisplay表示
setInterval(function () {
  req.open('GET', 'data/data.csv', true);
  req.send(null);
}, 200)