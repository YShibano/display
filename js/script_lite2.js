/** 日付表示用 */
/* (時計モジュールにて本体の時計がリセットされているため、本体から時間を取得) */
function LoadProc(now) {
  var target = document.getElementById("time");
  var Year = now.getFullYear();var Month = now.getMonth() + 1;var Date = now.getDate();
  var Weekday = now.getDay();//曜日(0～6=日～土)
  var Hour = ('00'+now.getHours()).slice(-2);
  var Min = ('00'+now.getMinutes()).slice(-2);
  var Sec = ('00'+now.getSeconds()).slice(-2);

  /* 曜日の選択肢 */
  var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

  /* 月名の選択肢 */
  var month = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

  //target.innerHTML = Year + "年" + Month + "月" + Date + "日" + Hour + ":" + Min + ":" + Sec;
  target.innerHTML =  Hour + ":" + Min + ":" + Sec;
  //target.innerHTML = month[Month - 1] + "/" + Date + "/" + Year + "(" + weekday[Weekday] + ")";
}
/** 数字判定 */
function is_num(str) {
  const pattern = /^[+,-]?([1-9]\d*|0)(\.\d+)?$/
  return pattern.test(str);
}

/** 最小値 */
function fmin(list){
  var min=list[0];
  for(var i = 0+1;i < list.length;i++){
    if(list[i]<min )min=list[i];
  }
  return min;
}
/** 最大値 */
function fmax(list){
  var max=list[0];
  for(var i = 0+1;i < list.length;i++){
    if(list[i]>max )max=list[i];
  }
  return max;
}

/**
 * 0以上の整数の頭に0を追加して、指定した桁数にする
 * @param {number} num   - 桁数を調整する数値
 * @param {number} digit - 桁数
 */
function adjust_digits(num, digit) {
  var no = String(num);
  while(no.length < digit) {
      no = ' ' + no;
  }
  return no;
}


/** メーター色塗り用関数 */
function drawmeter(value){
  /* change color */
  if(value>=1.00){
    for(var i=0; i<40;i++){//ALL GREEN 
      sell1=document.getElementById('left'+i);
      sell2=document.getElementById('right'+i);
      sell1.classList.remove('fadeout');
      sell2.classList.remove('fadeout');
      sell1.classList.add('fadein');
      sell2.classList.add('fadein');
      sell1.style.backgroundColor = "#96ff96";
      sell2.style.backgroundColor = "#96ff96";
    }
  }else if(value<0.05){//ALL RED
    for(var i=0; i<40;i++){
      sell1=document.getElementById('left'+i);
      sell2=document.getElementById('right'+i);
      sell1.classList.remove('fadein');
      sell2.classList.remove('fadein');
      sell1.classList.add('fadeout');
      sell2.classList.add('fadeout');
      sell1.style.backgroundColor = "#ff0000";
      sell2.style.backgroundColor = "#ff0000";
    }
  }else{
    var drawarea = (value)/0.025;
    for(var i=0; i<40;i++){
      if(i<drawarea-1){
        sell1=document.getElementById('left'+i);
        sell2=document.getElementById('right'+i);
        sell1.classList.remove('fadeout');
        sell2.classList.remove('fadeout');
        sell1.classList.add('fadein');
        sell2.classList.add('fadein');
        sell1.style.backgroundColor = "#96ff96";
        sell2.style.backgroundColor = "#96ff96";
      }else{
        sell1=document.getElementById('left'+i);
        sell2=document.getElementById('right'+i);
        sell1.classList.remove('fadein');
        sell2.classList.remove('fadein');
        sell1.classList.add('fadeout');
        sell2.classList.add('fadeout');
        sell1.style.backgroundColor = "#ff0000";
        sell2.style.backgroundColor = "#ff0000";
      }
    }
  }
}

/** 数値判定 */
function is_num(str) {
  const pattern = /^[+,-]?([1-9]\d*|0)(\.\d+)?$/
  return pattern.test(str);
}

/** センサー値取得 */
function get_sensordata(values, sensorNum, tmp, offset) {
  for (var i = 0; i < sensorNum; i++) {

    if ((i + offset) < tmp.length && is_num(tmp[i + offset])) {
      values[i] = parseFloat(tmp[i + offset]);
    }
    // return values;
  }
}


/****************/
/** 隠しコマンド */
/****************/
// 高度生値
function hiddendisp() {
  if(hiddenalt_flg){
    target.classList.remove('fadein');
    target.classList.add('fadeout');
    hiddenalt_flg = 0;
  }else{
    target.classList.remove('fadeout');
    target.classList.add('fadein');
    hiddenalt_flg = 1;
  }
}
// log読み込み停止(厳密には止まらない)
function hiddenstopper() {
  if(stop_flg){
    stopper.classList.remove('fadein');
    stopper.classList.add('fadeout');
    stop_flg=0;
  }else{
    stopper.classList.remove('fadeout');
    stopper.classList.add('fadein');
    stop_flg=1;
  }
}


/************/
/** 定数定義 */
/************/
/**電源温度関連**/
const CBatteryTempMaxNum = 4;
const CthBatTmpSteady = 20; //■常時フライト前
const CthBatTmpUsual = 30; //平常ホバリング
const CthBatTmpCaution = 40; //注意
const CthBatTmpWorning = 60; //警告
const CthBatTmpDenger = 80; //危険

/**プロペラ回転数関連**/
const CPropellerNum = 8;
const CthRpmSteady = 1000; //■常時フライト前
const CthRpmUsual = 2000; //平常ホバリング
const CthRpmCaution = 2500; //注意
/** 使用中 **/
const CthRpmWorning = 2600; //警告 黄
const CthRpmDenger = 2700; //危険 赤

/** 高度計 **/
const CAltDispTypeMaxNum = 1;//表示種別数
const CAltDispMinValue = 0;
const CAltDispMaxValue = 1;
const CAltDispHisDown = 0.20; //着陸or離陸 赤
const CAltDispHisUp = 0.30; //着陸or離陸 赤

/** 電源 **/
const CBatteryMaxNum = 8; //バッテリー最大数

/** 電流値 **/
const CDispCurMaxValue = 220; //表示用最大値
const CDispCurMinValue = 0;   //表示用最小値
const CthCurSteady = 20;  //■常時フライト前
const CthCurUsual = 90;  //平常ホバリング
const CthCurCaution = 120; //注意
/** 使用中 **/
const CthCurWorning = 200; //警告 黄
const CthCurDenger = 220;  //危険 

/**電圧モニター関連**/
const CDispVMaxValue = 105; //表示用最大値
const CDispVMinValue = 95;  //表示用最小値
const CthVoltSteady = 103; //■常時フライト前
const CthVoltUsual = 98;  //平常ホバリング
const CthVoltCaution = 95;  //注意
/** 使用中 **/
const CthVoltWorning = 88;  //警告
const CthVoltDenger = 86;  //危険
const CAtlMaxNUM = 1;

/**************************/
//  ●●●●各種センサーの通信と
/**************************/
/* 列番号(0始まり：カラム数+1) */
const ofst_Vol_Temp = 3;
const ofst_Rpm = 7;
const ofst_Vol = 15;
const ofst_Cur = 23;
const ofst_Alt = 36;
//ofst_Alt(高度値の位置)31:センサー1, 34:後方高度, 35:前方高度, 36:中心高度, 37:role角, 38:pitch角

/* リングバッファ用定数 */
/* for 高度計 */
const UseAltRingBf = 1;
const UseAltSupportForLanding = 1;
const UseAltSupportForOutlier = 0;
const ThAltOutlierAbsValue = 0.50;
const AltRingBfSize = 20;
const AltAveRange = 10;

//変数宣言
var Time = [];
var VatTemps = [];
var Currents = [];
var Voltages = [];
var Rpms = [];
var Altitudes = [];
var snsrTypes = {
                  Temp   : 0,
                  rpm    : 1,
                  volt   : 2,
                  curr   : 3,
                  Alti   : 4,
                  typeNum: 5,
                };
var eSensorNum = [CBatteryTempMaxNum, CPropellerNum, CBatteryMaxNum, CBatteryMaxNum, CAltDispTypeMaxNum];
var eOffset = [ofst_Vol_Temp, ofst_Rpm, ofst_Vol, ofst_Cur, ofst_Alt];
var altitude_pre=Number.MAX_SAFE_INTEGER;//初期値無効値
var alt_denger_flg=0;

var target = document.getElementById('hiddenroom');
var hiddenalt_flg = 0;
var stopper = document.getElementById('hiddenstopper');
var stop_flg = 0;

/************* */
/*メインロジック*/
/************* */
/*高度用リングバッファobjectのインスタンス化*/
var altringbf = new RingBuffer(AltRingBfSize);
var alt_ring_avg_pre = 0.00;

/*HTTPリクエスト用objectのインスタンス化*/
var req = new XMLHttpRequest();

req.onreadystatechange = function () {
  if (req.readyState == 4) { // 通信の完了時
    if (req.status <= 200) { // 通信の成功時
      let val = req.responseText;
      if (!val || val.length <= 0) {
        return
      }
      //csvデータをカンマ区切りで指定位置から読み込む
      let tmp = val.split(',');
      get_sensordata(VatTemps, eSensorNum[snsrTypes.Temp], tmp, eOffset[snsrTypes.Temp]);
      get_sensordata(Rpms, eSensorNum[snsrTypes.rpm], tmp, eOffset[snsrTypes.rpm]);
      get_sensordata(Voltages, eSensorNum[snsrTypes.volt], tmp, eOffset[snsrTypes.volt]);
      get_sensordata(Currents, eSensorNum[snsrTypes.curr], tmp, eOffset[snsrTypes.curr]);
      get_sensordata(Altitudes, eSensorNum[snsrTypes.Alti], tmp, eOffset[snsrTypes.Alti]);

      /************************* */
      /* 時計の表示用関数呼び出し */
      /************************* */
      LoadProc(new Date);

      /************* */
      /* 高度計の表示 */
      /************* */
      var altValue = 0.0;
      if (UseAltRingBf){/* リングバッファによる平均値 */

        //値飛び防止ロジック(プロペラ回転数による0cm判定)

        //A.着陸時対策
        if(UseAltSupportForLanding){
          altValue = (Altitudes[0] < 0.00 || fmax(Rpms) < 500)? 0.00 : Altitudes[0];
          //console.log("A");
        }

        //B.外れ値対策(※要検討)
        if(UseAltSupportForOutlier && (Math.abs(alt_ring_avg_pre - Altitudes[0]) > ThAltOutlierAbsValue)){
          altValue = alt_ring_avg_pre; //前周期の平均値
          //console.log("B");
        }
        
        //C.対策なし
        if(!UseAltSupportForLanding && !UseAltSupportForOutlier){
          altValue = Altitudes[0];
          //console.log("C");
        }

        /* DEBUG */
        //console.log("▼getAverage()▼");console.log("Cur："+altValue);

        //値を格納
        altringbf.add(altValue);

        //平均値算出
        altValue = altringbf.getAverage(AltAveRange);

        /* 表示変更 */
        document.getElementById("up-c-2").innerText = altValue.toFixed(2);

        //前回値保持
        alt_ring_avg_pre = altValue;

      }else{/* 生値 */
        if(Altitudes[0] < 0.00){/* マイナス値表示への対応 */
          document.getElementById("up-c-2").innerText = "0.00";
        }else{
          document.getElementById("up-c-2").innerText = Altitudes[0].toFixed(2);
        }
        altValue = Altitudes[0];
      }

      /* DEBUG */
      document.getElementById("hiddenroom").innerText = Altitudes[0].toFixed(2);

      /* メーター表示を反映 */
      drawmeter(altValue);

      /* 上段中央のclass要素を取得 */
      /* ※現状1つしかないため、以降elementsに[0]を指定 */
      var elements = document.getElementsByClassName('up-center');

      // シンプルカラーチェンジ 
      if(altValue<=CAltDispHisDown){
        elements[0].style.backgroundColor="#FF0000";
      }else{
        elements[0].style.backgroundColor="#3c3c3c";
      }

      // ヒステリシスカラーチェンジ(ToDo)

      //温度
      document.getElementById("lw-itm-4-elem").innerText = fmax(VatTemps).toFixed(1);

      //電流
      document.getElementById("lw-itm-2-elem").innerText = fmax(Currents).toFixed(0);
      var elements = document.getElementsByClassName('lw-itm-2');
      if(fmax(Currents)>=CthCurDenger){
        elements[0].style.backgroundColor="#FF0000";
      }else if(fmax(Currents)>=CthCurWorning){
        elements[0].style.backgroundColor="#FFFF00";
      }else{
        elements[0].style.backgroundColor="#3c3c3c";
      }

      //電圧
      document.getElementById("lw-itm-1-elem").innerText = fmin(Voltages).toFixed(1);

      var elements = document.getElementsByClassName('lw-itm-1');
      if(fmin(Voltages)<=CthVoltDenger){
        elements[0].style.backgroundColor="#FF0000";
      }else if(fmin(Voltages)<=CthVoltWorning){
        elements[0].style.backgroundColor="#FFFF00";
      }else{
        elements[0].style.backgroundColor="#3c3c3c";
      }

      //プロペラ回転速度
      document.getElementById("lw-itm-3-elem").innerText = fmax(Rpms).toFixed(0);

      var elements = document.getElementsByClassName('lw-itm-3');
      if(fmax(Rpms)>=CthRpmDenger){
        elements[0].style.backgroundColor="#FF0000";
      }else if(fmax(Rpms)>=CthRpmWorning){
        elements[0].style.backgroundColor="#FFFF00";
      }else{
        elements[0].style.backgroundColor="#3c3c3c";
      }
      
    }
  }
}

//隠し部屋
target.addEventListener('click', hiddendisp, false);
stopper.addEventListener('click', hiddenstopper, false);

// data.csvに書き込んだデータをDisplay表示
setInterval(function () {
  req.open('GET', 'data/data.csv', true); 
  if(stop_flg){
    return;
  }
  req.send(null);
}, 100)
