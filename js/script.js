// document.write("<script src='zeu.js'></script>");

// window.onload = function(){alert(resText);};

const skydrive_color = '#0e1b9e';

/**電源温度関連**/
const CBatteryTempMaxNum = 4;
const CthBatTmpSteady=20;        //■常時フライト前
const CthBatTmpUsual=30;         //平常ホバリング
const CthBatTmpCaution=40;      //注意
const CthBatTmpWorning=60;     //警告
const CthBatTmpDenger=80;      //危険

/**プロペラ回転数関連**/
const CPropellerNum = 8;
const CthRpmSteady=1000;        //■常時フライト前
const CthRpmUsual=2000;         //平常ホバリング
const CthRpmCaution=2500;      //注意
const CthRpmWorning=3000;     //警告
const CthRpmDenger=3500;      //危険

/** 高度計 **/
const CAltDispTypeMaxNum = 1;//表示種別数
const CAltDispMinValue = 0;
const CAltDispMaxValue = 1;

/** 電源 **/
const CBatteryMaxNum = 8; //バッテリー最大数
/** 電流値 **/
const CDispCurMaxValue=220;//表示用最大値
const CDispCurMinValue=0;//表示用最小値
const CthCurSteady=20;        //■常時フライト前
const CthCurUsual=90;         //平常ホバリング
const CthCurCaution=120;      //注意
const CthCurWorning=150;     //警告
const CthCurDenger=200;      //危険

/**電圧モニター関連**/
const CDispVMaxValue=105; //表示用最大値
const CDispVMinValue=95;    //表示用最小値
const CthVoltSteady=103;     //■常時フライト前
const CthVoltUsual=98;         //平常ホバリング
const CthVoltCaution=95;      //注意
const CthVoltWorning=93;     //警告
const CthVoltDenger=90;      //危険


function is_num(str) {
  const pattern = /^[+,-]?([1-9]\d*|0)(\.\d+)?$/
  return pattern.test(str);
}

/**************************/
/**************************/
// Each Contents Settings
/**************************/
/**************************/
/* TextMeter Options */
var TMoptions = {
  
  arrowWidth:50,
  arrowSpeed:0.3,
  viewWidth: 500,
  value: 0, // Percentage number value. Range 0 to 100.
  displayValue: 'TEST', // Display text.
  arrowColor: '#007bfb', // Arrow color.
  
  marker: {// Marker that shows percentage number.
    bgColor: '#343a42',// Background color.
    fontColor: '#ffffff' // Font color.
  },

  bar: { // Bar that has text inside.
    speed: 5,// Scrolling speed.
    fillColor: '#dc3547',// Bar fill color.
    bgColor: '#f8f8ff',// Bar background color.
    borderColor: '#343a42'// Border line color.
  }
}

/* CurrentBarMeter Options */
var CbmOptions = {
  min: {// Minimum value at the bottom
    fontColor: '#ffffff',// Font color.
    fontSize: 60,
    value:CDispCurMinValue,// Number value.
    bgColor: COLOR.black2,//'#000000' // Background color.
  },
  max: {// Maximum value at the top.
    fontColor: '#ffffff',// Font color.
    fontSize:60,
    value: CDispCurMaxValue,// Number value.
    bgColor: COLOR.black2,//'#000000' // Background color.
  },
  bar: {// Bar,
    borderColor: COLOR.black2,//'#000000',// Border color.
    // bgColor:'#cccccc',
    graident: true, // Bar color gradieient or not.
    speed: 5// Scrolling speed.
  },
  marker: {  // Marker,
    bgColor: '#28a748', // Background color.
    fontColor: '#ffffff',// Font color.
    fontSize: 5
  },
  value: 0  // Actual number value.
}


/* VolumeMeter Options */
var VmOptions = {
  min: {// Minimum value at the bottom
    fontColor: '#ffffff',// Font color.
    fontSize: 60,
    value: CDispVMinValue,// Number value.
    bgColor: COLOR.black2,//'#000000' // Background color.
  },
  max: {// Maximum value at the top.
    fontColor: '#ffffff',// Font color.
    fontSize:60,
    value: CDispVMaxValue,// Number value.
    bgColor: COLOR.black2,//'#000000' // Background color.
  },
  bar: {// Bar,
    borderColor: COLOR.black2,//'#000000',// Border color.
    bgColor:'#cccccc',
    graident: true, // Bar color gradieient or not.
    speed: 5// Scrolling speed.

  },
  marker: {  // Marker,
    bgColor: '#28a748', // Background color.
    fontColor: '#ffffff',// Font color.
    fontSize: 10
  },
  value: 0  // Actual number value.
}


/* AltitudeMeter Options */
var AmOptions = {
  min: {// Minimum value at the bottom
    fontColor: '#ffffff',// Font color.
    fontSize: 60,
    value: CAltDispMinValue,// Number value.
    bgColor: COLOR.black2,//'#000000' // Background color.
  },
  max: {// Maximum value at the top.
    fontColor: '#ffffff',// Font color.
    fontSize:60,
    value: CAltDispMaxValue,// Number value.
    bgColor: COLOR.black2,//'#000000' // Background color.
  },
  bar: {// Bar,
    borderColor: COLOR.black2,//'#000000',// Border color.
    bgColor:'#cccccc',
    graident: true, // Bar color gradieient or not.
    speed: 5// Scrolling speed.

  },
  marker: {  // Marker,
    bgColor: '#28a748', // Background color.
    fontColor: '#ffffff',// Font color.
    fontSize: 10
  },
  value: 0  // Actual number value.
}



//Rpm Queues
/* Options */
var MQoptions = {
  viewWidth: 130,//
  viewHeight: 100,
  barHeight: 10, // Bar height number.
  space: 5,  // Vertical space between two bars.
  speed: 10,// Bar scolling speed.
  maxQueueCapacity: 7// The maximum number of elements can be hold in the queue.
};

// Visualized fan (仮)
/* Options */
var RFoptions = {
  fanColor: '#00d7af', // Fan color.
  center: {
    color: '#00d7af',// The center circle color.
    bgColor: '#FFFFFF'// The center circle 2 color.
  },

  speed: 1// Fan speed. The larger, the faster.
}


/****/
//Degital-Clock
/****/
var digitalClock = new zeu.DigitalClock('digital-clock', {
  // numberColor: "#00ff00",
  // dashColor: COLOR.black2
  numberColor: COLOR.timemain,
  dashColor: COLOR.timedash
  // hourOffset:0
});
digitalClock.scaleByHeight(50);


/**************************/
/**************************/
// ●●●●Battery Degree Display
/**************************/
/**************************/
var textMeters = [];
// タイプを定義
for (var i = 0; i < CBatteryTempMaxNum; i++) {
  textMeters[i] = new zeu.TextMeter('text-meter-' + (i + 1), TMoptions);
  // データを定義
  textMeters[i].displayValue = 'Battery ' + (i + 1);
  textMeters[i].value = 0;
  textMeters[i].speed = 5;
  textMeters[i].fillColor = '#dc3547';
  textMeters[i].bgColor = '#f8f8ff';
  textMeters[i].arrowColor = '#007bfb';
  textMeters[i].markerFontColor = '#ffffff';
  textMeters[i].markerBgColor = '#343a42';
}

/**************************/
/**************************/
// ●●●●Propeller Rpm display
/**************************/
/**************************/
/* Constructor */
// var roundFan = [];
// for (var i = 1; i <= CPropellerNum; i++) {
//   roundFan[i] = new zeu.RoundFan('round-fan-' + i, RFoptions);

//   /* Setter */
//   roundFan[i].speed = 10;
//   roundFan[i].fanColor = skydrive_color;
//   roundFan[i].centerColor = skydrive_color;
//   roundFan[i].centerBgColor = '#FFFFFF';
// }


//rpm/current text
var rpmTextBoxes = [];
for (var i = 1; i <= CPropellerNum; i++) {
  var rpmTextBox = new zeu.TextBox('rpm-textbox-' + i, {
    text: {
      fontColor: COLOR.white
    },
    bgColor: COLOR.transparent,
    waveColor: COLOR.transparent
  });
  rpmTextBox.scaleByHeight(40);
  rpmTextBoxes.push(rpmTextBox);
}


var CurBarMeters = [];
for (var i = 0; i < CBatteryMaxNum; i++) {
  CurBarMeters[i] = new zeu.VolumeMeter('current-meter-' + (i + 1), CbmOptions);
  CurBarMeters[i].viewHeight = 50;
  CurBarMeters[i]._unit =' A';
}

//Current I
var curTextBoxes = [];
for (var i = 1; i <= CBatteryMaxNum; i++) {
  var curTextBox = new zeu.TextBox('current-textbox-' + i, {
    text: {
      fontColor: COLOR.white,
      bgColor: COLOR.green,
      value: 0
    },
    borderColor: COLOR.transparent,
    bgColor: COLOR.transparent,
    waveColor: COLOR.transparent,
    borderColor: '#505050',
    bgColor: 'rbga(50,50,50,0.2)',
  });
  curTextBox.scaleByHeight(40);
  curTextBoxes.push(curTextBox);
}

/**************************/
/**************************/
//  ●●●●Voltage Meters display
/**************************/
/**************************/
var volMeters = [];
  //電圧メーター
for (var i = 0; i < CBatteryMaxNum; i++) {
  volMeters[i] = new zeu.VolumeMeter('voltage-meter-' + (i + 1), VmOptions);
  volMeters[i].markerBgColor='#aa0022';
  volMeters[i].viewHeight = 50;
  volMeters[i]._unit =' V';
}

var volTextBoxes = [];
for (var i = 1; i <= CBatteryMaxNum; i++) {
  //電圧数値表示
  var volTextBox = new zeu.TextBox('voltage-textbox-' + i, {
    text: {
      fontColor: COLOR.white,
      bgColor: COLOR.green,
      value: 0
    },
    borderColor: COLOR.transparent,
    bgColor: COLOR.transparent,
    waveColor: COLOR.transparent,
    borderColor: '#505050',
    bgColor: 'rbga(50,50,50,0.2)'
  });
  volTextBox.scaleByHeight(40);
  volTextBoxes.push(volTextBox);
}

/**************************/
/**************************/
//  ●●●●Altitude Meters display
/**************************/
/**************************/
var altMeters = [];
  //高度計
for (var i = 0; i < CAltDispTypeMaxNum; i++) {
  altMeters[i] = new zeu.VolumeMeter('Altitude-meter-' + (i + 1), AmOptions);
  altMeters[i].markerBgColor='#aa0022';
  altMeters[i].viewHeight = 50;
  altMeters[i]._unit =' m';
}

/**************************/
//  ●●●●各種センサーの通信と
/**************************/
//列番号(0始まり：カラム数+1)
const ofst_Vol_Temp=3;
const ofst_Rpm=7;
const ofst_Vol=15;
const ofst_Cur=23;
const ofst_Alt=36;
//ofst_Alt(高度値の位置)31:センサー1, 34:後方高度, 35:前方高度, 36:中心高度, 37:role角, 38:pitch角

var req = new XMLHttpRequest();
req.onreadystatechange = function () {
  if (req.readyState == 4) { // 通信の完了時
    if (req.status <= 200) { // 通信の成功時
      let val = req.responseText;
      if (!val || val.length <= 0) {
        return
      }

      let tmp = val.split(',');
      let offset;
      console.log(tmp);

/***************** */
      // DisplayArea1
/***************** */

      //■電源温度
      offset = ofst_Vol_Temp;
      for (var i = 0; i < CBatteryTempMaxNum; i++) {
        if ((i + offset) < tmp.length && is_num(tmp[i + offset])) {
          textMeters[i].value = (tmp[i + offset]);
          textMeters[i].displayValue = parseFloat(tmp[i + offset]) + " Deg";
        }
        //statusをVisualize
        if (parseFloat(tmp[offset + i]) >= CthBatTmpDenger) {//危険
          textMeters[i].fillColor = COLOR.black2;
          textMeters[i].fontColor = COLOR.black2;
          textMeters[i].arrowColor = COLOR.black2;
        } else if (parseFloat(tmp[offset + i]) >= CthBatTmpWorning) {//警告
          textMeters[i].fillColor = COLOR.red;
          textMeters[i].fontColor = COLOR.red;
          textMeters[i].arrowColor = COLOR.red;
        } else if (parseFloat(tmp[offset + i]) >= CthBatTmpCaution) {//注意
          textMeters[i].fillColor = COLOR.orange;
          textMeters[i].fontColor = COLOR.orange;
           textMeters[i].arrowColor = COLOR.orange;
        } else if (parseFloat(tmp[offset + i]) >= CthBatTmpUsual) {//平常ホバリング
          textMeters[i].fillColor= COLOR.green;
          textMeters[i].fontColor = COLOR.green;
          textMeters[i].arrowColor = COLOR.green;
        } else if (parseFloat(tmp[offset + i]) <= CthBatTmpSteady) {//■常時フライト前
          textMeters[i].fillColor = COLOR.skydrive_blue;
          textMeters[i].fontColor = COLOR.skydrive_blue;
          textMeters[i].arrowColor = COLOR.blacskydrive_bluek2;
        } else {//■常時フライト前～平常ホバリング
          textMeters[i].fillColor = COLOR.cyan;
          textMeters[i].fontColor = COLOR.cyan;
          textMeters[i].arrowColor = COLOR.cyan;
        }
      }


      //■プロペラ回転速度 / 電流
      for (var i = 0; i < CPropellerNum; i++) {
        //■プロペラ回転速度
        offset = ofst_Rpm;
        if ((i + offset) < tmp.length && is_num(tmp[i + offset])) {
          rpmTextBoxes[i].value = parseInt(tmp[i + offset]) + " rpm";
        }
        if (parseFloat(tmp[offset + i]) >= CthRpmDenger) {//危険
          rpmTextBoxes[i].textBgColor = COLOR.black2;
        } else if (parseFloat(tmp[offset + i]) >= CthRpmWorning) {//警告
          rpmTextBoxes[i].textBgColor = COLOR.red;
        } else if (parseFloat(tmp[offset + i]) >= CthRpmCaution) {//注意
          rpmTextBoxes[i].textBgColor = COLOR.orange;
        } else if (parseFloat(tmp[offset + i]) >= CthRpmUsual) {//平常ホバリング
          rpmTextBoxes[i].textBgColor = COLOR.green;
        } else if (parseFloat(tmp[offset + i]) >= CthRpmSteady) {//■常時フライト前⇒平常ホバリング
          rpmTextBoxes[i].textBgColor = COLOR.skydrive_blue;
        }  else if (parseFloat(tmp[offset + i]) >= 500) {//■常時フライト前
          rpmTextBoxes[i].textBgColor = COLOR.skydrive_blue;
        } else {//常時フライト前～平常ホバリング
          rpmTextBoxes[i].textBgColor = COLOR.gray;
        }

        //■プロペラ回転速度ファン
        //var fanRpm = document.getElementById("prop-rpm-" + (i + 1));
        // if ((i + offset) < tmp.length && is_num(tmp[i + offset])) {
        //   roundFan[i].speed = parseInt(tmp[offset + i]);
        // }

        //■電流計
        offset = ofst_Cur;//電流
        if ((i + offset) < tmp.length && is_num(tmp[i + offset])) {
          CurBarMeters[i].value = parseFloat(tmp[i + offset]);
          curTextBoxes[i].value = parseFloat(tmp[i + offset]) + " A";
        }
        if (parseFloat(tmp[offset + i]) >= CthCurDenger) {//危険
          CurBarMeters[i].barFillColor = "#000000";//COLOR.black2;
          curTextBoxes[i].textBgColor = COLOR.black2;
        } else if (parseFloat(tmp[offset + i]) >= CthCurWorning) {//警告
          CurBarMeters[i].barFillColor =COLOR.red;
          curTextBoxes[i].textBgColor = COLOR.red;
        } else if (parseFloat(tmp[offset + i]) >= CthCurCaution) {//注意
          CurBarMeters[i].barFillColor =COLOR.orange;
          curTextBoxes[i].textBgColor = COLOR.orange;
        } else if (parseFloat(tmp[offset + i]) >= CthCurUsual) {//平常ホバリング
          CurBarMeters[i].barFillColor =COLOR.green;
          curTextBoxes[i].textBgColor = COLOR.green;
        } else if (parseFloat(tmp[offset + i]) <= CthCurSteady) {//■常時フライト前
          CurBarMeters[i].barFillColor =COLOR.skydrive_blue;
          curTextBoxes[i].textBgColor = COLOR.skydrive_blue;
        } else {//■常時フライト前～平常ホバリング
          CurBarMeters[i].barFillColor =COLOR.cyan;
          curTextBoxes[i].textBgColor = COLOR.cyan;
        }
      }

      offset = ofst_Vol;
      for (var i = 0; i < CBatteryMaxNum; i++) {
        if ((i + offset) < tmp.length && is_num(tmp[i + offset])) {
          volMeters[i].value = parseFloat(tmp[i + offset]);
          volTextBoxes[i].value = parseFloat(tmp[i + offset]) + " V";
        }
        if (parseInt(tmp[offset + i]) >= CthVoltSteady) {//■常時フライト前
          volMeters[i].barFillColor =COLOR.skydrive_blue;
          volTextBoxes[i].textBgColor = COLOR.skydrive_blue;
        } else if (parseInt(tmp[offset + i]) >= CthVoltUsual) {//平常ホバリング
          volMeters[i].barFillColor =COLOR.green;
          volTextBoxes[i].textBgColor = COLOR.green;
        } else if (parseInt(tmp[offset + i]) >= CthVoltCaution) {//注意
          volMeters[i].barFillColor =COLOR.orange;
          volTextBoxes[i].textBgColor = COLOR.orange;
        } else if (parseInt(tmp[offset + i]) >= CthVoltWorning) {//警告
          volMeters[i].barFillColor =COLOR.red;
          volTextBoxes[i].textBgColor = COLOR.red;
        } else if (parseInt(tmp[offset + i]) <= CthVoltDenger){ //危険
          volMeters[i].barFillColor =COLOR.black;
          volTextBoxes[i].textBgColor = COLOR.black;
          volMeters[i]._isWaveOn = true;
          volTextBoxes[i]._isWaveOn = true;
        } else {
          volMeters[i].barFillColor =COLOR.cyan;
          volTextBoxes[i].textBgColor = COLOR.cyan;//COLOR.yellow;
        }
      }

      /***************** */
            // DisplayArea2(センサーグラフ用のjsに記載)
      /***************** */

      offset = ofst_Alt;
      for (var i = 0; i < CAltDispTypeMaxNum; i++) {
        if ((i + offset) < tmp.length && is_num(tmp[i + offset])) {
          altMeters[i].value = parseFloat(tmp[i + offset]/100);
        }
        altMeters[i]._isWaveOn = true;
        if (parseInt(tmp[offset + i]) < 0.2) {//■常時フライト前
         altMeters[i].barFillColor =COLOR.skydrive_blue;
        } else if (parseInt(tmp[offset + i]) >= 1) {//平常ホバリング
         altMeters[i].barFillColor =COLOR.green;
        } else if (parseInt(tmp[offset + i]) >= 0.75) {//注意
         altMeters[i].barFillColor =COLOR.orange;
        } else if (parseInt(tmp[offset + i]) >= 0.5) {//警告
         altMeters[i].barFillColor =COLOR.red;
        } else if (parseInt(tmp[offset + i]) >= 0.2){ //危険
         altMeters[i].barFillColor =COLOR.black;
         altMeters[i]._isWaveOn = true;
        } else {
         altMeters[i].barFillColor =COLOR.cyan;
        }
      }

      //高度計：値表示
      var center = altMeters[0].value*100;
      var strcenter= center.toFixed(2);
      document.getElementById("centerAltitude").innerText = "Alt：" + strcenter.toString() + " cm";
      //center;//String(altMeters[0].value);//center.toString();
      //"Alt：" + center.toString() + " m";
      
      // var value = Math.round((altMeters[2].value * 10)/10);
      //var value = center.toFixed(3);
      //var center=altMeters[2].value;
      //elm.textContent = 'Alt：' + value.toString() + ' m';
      //elm.textContent = 'Alt：' + altMeters[2].value + ' [m]';
      // elm.style.background = '#ffcccc';
    } 
  }
}


// 
// data.csvに書き込んだデータをDisplay表示
// 
setInterval(function () {
  req.open('GET', 'data/data.csv', true);
  req.send(null);
}, 200)

