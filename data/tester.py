import pandas as pd
from time import sleep
import time

indata_path = 'flight01org_20200805.csv'
outdata_path = 'data.csv'

df_indata = pd.read_csv(indata_path, header=200, encoding='cp932')
#df_indata = pd.read_csv(indata_path, encoding='cp932')
#df_outdata = pd.read_csv(outdata_path, nrows=0, encoding='cp932')

try:
  for tupl in df_indata.itertuples(name=None):
    start = time.time()
    df_outdata=pd.DataFrame(tupl[1:]).T
    df_outdata.to_csv(outdata_path, encoding="utf-8", mode='w+', index=False, header=False)

    elapsed_time = time.time() - start
    print("{:.3f}".format(elapsed_time)+"[sec]")
    if(elapsed_time < 0.10):
      sleep(0.1-elapsed_time)#waittime:100msec
      
    total_fintime= time.time() - start
    print("{:.3f}".format(total_fintime)+"[sec]")
except KeyboardInterrupt:
  print("Abended by Ctrl+C") #abort:障害により異常終了 abend:異常終了した状態
print("end")